#!/usr/bin/env python3
import sys
import argparse
import shutil
import time
from random import randint
import platform
import socket
import multiprocessing
import logging
import subprocess
import json
from datetime import datetime

RW_MAP = {
    'randread': {
        'direction': ['read'],
        'bs': '128k',
        'iodepth': '128'
    },
    'read': {
        'direction': ['read'],
        'bs': '128k',
        'iodepth': '128'
    },
    'randwrite': {
        'direction': ['write'],
        'bs': '512k',
        'iodepth': '128'
    },
    'write': {
        'direction': ['write'],
        'bs': '512k',
        'iodepth': '128'
    }
}


def telegraf_join_dict(target):
    items = []
    for k, v in target.items():
        items.append("%s=%s" % (k, v))
    return ",".join(items)


def parse_args():

    parser = argparse.ArgumentParser(
        description='Write out file and output timing info')
    parser.add_argument('-l', '--label', help='Custom label for metric')
    parser.add_argument(
        '-d',
        '--dry-run',
        action='store_true',
        help="Only print the command that would run, don't actually run it")
    parser.add_argument('-v',
                        '--verbose',
                        action='store_true',
                        help='Verbose output')
    parser.add_argument('-r',
                        '--runtime',
                        type=int,
                        help='Number of seconds to run for')
    parser.add_argument('-s',
                        '--splay',
                        help='Custom splay to spread the load (in seconds)',
                        default=1800,
                        type=int)
    parser.add_argument('-e',
                        '--ioengine',
                        nargs='+',
                        choices=['libaio', 'sync'])
    parser.add_argument('-i',
                        '--size',
                        type=str,
                        help='Size of test',
                        default='40G')
    parser.add_argument('target_dir',
                        help='Target directory to store test metric')

    return parser.parse_args()


def flatten(init, lkey=''):
    """
    TY Stack overflow:
    https://stackoverflow.com/questions/6027558/flatten-nested-dictionaries-compressing-keys
    """
    ret = {}
    for rkey, val in init.items():
        key = lkey + rkey
        if isinstance(val, dict):
            ret.update(flatten(val, key + '_'))
        else:
            ret[key] = val
    return ret


def build_fio_command(*args, **kwargs):
    """
    Args:
      target_dir (str): Target directory to perform tests on
      readwrite (str) : Mode to run test in, should be one of:
                          'randread', 'read', 'randwrite', 'write'
    Params:
      ioengine (str) : Engine to run IO tests using.  Should be one of:
                         'libaio', 'sync'
      runtime (int)  : Time for test to run.  Default: 30
      size (str)     : Size of IO test.  Default: 40G
    """
    fio_bin = shutil.which("fio")
    target_dir = args[0]
    readwrite = args[1]
    target_host = socket.gethostname()
    num_cpus = int(multiprocessing.cpu_count())

    mappings = RW_MAP.get(readwrite)
    block_size = mappings.get('bs')
    iodepth = mappings.get('iodepth')
    ioengine = kwargs.get('ioengine')
    runtime = kwargs.get('runtime', 30)
    size = kwargs.get('size', '40G')

    if ioengine == 'sync':
        direct = 1
    elif ioengine == 'libaio':
        direct = 0
    else:
        raise Exception("BadIOEngine", "Invalid ioengine given")

    command_list = [
        fio_bin,
        "--filename=%s/%s.fio_testfile" % (target_dir, target_host),
        "--ioengine=%s" % ioengine,
        "--direct=%s" % direct, "--refill_buffers",
        "--readwrite=%s" % readwrite, "--norandommap", "--randrepeat=0",
        "--name=duke_test",
        "--bs=%s" % block_size,
        "--size=%s" % size,
        "--iodepth=%s" % iodepth, "--numjobs=1",
        "--runtime=%s" % runtime, "--fallocate=none", "--group_reporting",
        "--output-format=json+"
    ]

    # GTOD flag no worky on mac
    if platform.system() not in ['Darwin']:
        command_list.append("--gtod_cpu=%s" % str(num_cpus - 1))

    return " ".join(command_list)


def parse_output(output):
    return_data = []
    jobs = json.loads(output)['jobs']
    for job in jobs:
        flat_data = flatten(job)
        logging.debug(flat_data)
        return_data.append(flat_data)
    return return_data


def main():
    args = parse_args()
    time.sleep(randint(0, args.splay))

    if not args.ioengine:
        target_ioengines = ['libaio']
    else:
        target_ioengines = args.ioengine
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    relevant_fields = [
        'read_bw',
        'write_bw',
        'read_clat_ns_min',
        'read_clat_ns_max',
        'read_clat_ns_mean',
        'write_clat_ns_min',
        'write_clat_ns_max',
        'write_clat_ns_mean',
    ]
    readwrites = ['read', 'randread', 'write', 'randwrite']

    for target_ioengine in target_ioengines:
        for readwrite in readwrites:
            cmd = build_fio_command(args.target_dir,
                                    readwrite,
                                    ioengine=target_ioengine,
                                    runtime=args.runtime,
                                    size=args.size)
            timestamp = datetime.now().strftime("%s")
            tags = {
                'target_dir': args.target_dir,
                'readwrite': readwrite,
                'size': args.size,
                'ioengine': target_ioengine
            }
            logging.debug(cmd)

            if not args.dry_run:
                result = subprocess.check_output(cmd, shell=True).decode()
                data = parse_output(result)
                fields = {}
                for job in data:
                    for k, v in job.items():

                        # Keep the keys looking like normal strings
                        k = k.replace('>=', 'gte_')
                        logging.debug("%s=%s" % (k, v))

                        # Only look at certain fields
                        if k not in relevant_fields:
                            logging.debug("Ignoring field: %s" % k)
                            continue

                        # Ignore zero values
                        if int(v) == 0:
                            continue

                        fields[k] = v
                metric_line = "fio_io,%s %s %s" % (telegraf_join_dict(tags),
                                                   telegraf_join_dict(fields),
                                                   timestamp)
                print(metric_line)
    return 0


if __name__ == "__main__":
    sys.exit(main())
