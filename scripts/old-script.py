#!/usr/bin/env python3
import sys
import subprocess
from random import randint
import argparse
import os
import socket
import math
import multiprocessing
import json
import syslog
import time
from datetime import datetime


def merge_two_dicts(x, y):
    z = x.copy()  # start with x's keys and values
    z.update(y)  # modifies z with y's keys and values & returns None
    return z


def parse_args():

    parser = argparse.ArgumentParser(
        description='Write out file and output timing info')
    parser.add_argument('-l', '--label', help='Custom label for metric')
    parser.add_argument('-s',
                        '--splay',
                        help='Custom splay to spread the load (in seconds)',
                        default=1800,
                        type=int)
    parser.add_argument('target_dir',
                        help='Target directory to store test metric')

    return parser.parse_args()


def main():
    args = parse_args()

    time.sleep(randint(0, args.splay))

    direction_map = {
        'randread': ['read'],
        'read': ['read'],
        'randwrite': ['write'],
        'write': ['write'],
    }

    bs_map = {
        'randread': '128k',
        'read': '128k',
        'randwrite': '512k',
        'write': '512k'
    }

    iodepth_map = {
        'randread': '128',
        'read': '128',
        'randwrite': '128',
        'write': '128'
    }

    relevant_fields = [
        'read_bw', 'write_bw',
        'read_clat_ns_min', 'read_clat_ns_max', 'read_clat_ns_mean', 'read_clat_ns_stddev',
        'write_clat_ns_min', 'write_clat_ns_max', 'write_clat_ns_mean', 'write_clat_ns_stddev',
    ]

    target_dir = os.path.abspath(args.target_dir)
    if not os.path.exists(target_dir):
        sys.stderr.write("%s does not exist\n" % target_dir)
        sys.exit(2)

    ignore_tags = ['filename_format', 'name']

    target_host = socket.gethostname()
    num_cpus = int(multiprocessing.cpu_count())

    # For <= 4 cpus
    # Changing this to a simple '1' per Victor
#   if num_cpus == 0:
#       num_jobs = 1
#   else:
#       num_jobs = int(math.floor(num_cpus / 4))
    num_jobs = 1

    # NB: Do read first, in order to ensure testing file gets created.
    for readwrite in ['read', 'randread', 'write', 'randwrite']:
        block_size = bs_map.get(readwrite)
        iodepth = iodepth_map.get(readwrite)
        command_list = [
            "/usr/bin/fio",
            "--filename=%s/%s.fio_testfile" % (target_dir, target_host),
            "--ioengine=libaio", "--direct=1", "--refill_buffers",
            "--readwrite=%s" % readwrite, "--norandommap", "--randrepeat=0",
            "--name=duke_test",
            "--bs=%s" % block_size, "--size=40G", "--iodepth=%s" % iodepth,
            "--numjobs=%s" % num_jobs, "--runtime=30",
            "--gtod_cpu=%s" % str(num_cpus - 1), "--fallocate=none",
            "--group_reporting", "--output-format=json+"
        ]
        syslog.syslog("Running: %s\n" % " ".join(command_list))
        output = subprocess.check_output(command_list)
        data = json.loads(output.decode('utf-8'))
        metrics = []

        for job in data['jobs']:
            all_options = merge_two_dicts(data['global options'],
                                          job['job options'])
            relevant_options = {'target_dir': target_dir}

            # Clean up tags
            for item in all_options.keys():
                if item not in ignore_tags:
                    relevant_options[item] = all_options[item]

            metric = {"tags": relevant_options, "name": "fio_io", "fields": {}}

            # for direction in ['read', 'write']:
            for direction in direction_map[readwrite]:
                for metric_name, metric_value in job[direction].items():
                    if metric_name == 'clat_ns':
                        for submetric_name, submetric_value in metric_value.items():
                            full_metric_name = "%s_%s_%s" % (direction, metric_name, submetric_name)
                            metric["fields"][full_metric_name] = submetric_value
                            metrics.append(metric)
                    elif not isinstance(metric_value, dict):
                        full_metric_name = '%s_%s' % (direction, metric_name)
                        metric["fields"][full_metric_name] = metric_value
                        metrics.append(metric)
        full_metrics = {'metrics': metrics}

        timestamp = datetime.now().strftime("%s")
        for metric in full_metrics['metrics']:
            fields = []
            tags = []

            # These are the actual metrics
            for field_n, field_v in metric['fields'].items():
                # Only look at certain fields
                if field_n in relevant_fields:
                    fields.append("%s=%s" % (field_n, field_v))

            # Tags/Labels
            for tag_n, tag_v in metric['tags'].items():
                tags.append("%s=%s" % (tag_n, tag_v))

            if args.label:
                tags.append("label=%s" % args.label)

            metric_line = 'fio_io,%s %s %s' % (",".join(tags),
                                               ",".join(fields), timestamp)
            print(metric_line)
            syslog.syslog(metric_line)

    return 0


if __name__ == "__main__":
    sys.exit(main())
